import { StatusBar } from "expo-status-bar";
import "react-native-reanimated";
import "react-native-gesture-handler";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  SafeAreaView,
  SafeAreaProvider,
  SafeAreaInsetsContext,
  useSafeAreaInsets,
  initialWindowMetrics,
} from "react-native-safe-area-context";
import Home from "./src/screens/home";
import {
  Feather,
  SimpleLineIcons,
  Octicons,
  FontAwesome,
} from "@expo/vector-icons";
import { useState } from "react";
import { CircleIcon, threeLine } from "./assets/images";
import Auth from "./src/screens/auth";
import MainRoute from "./src/routing";
import Toast from "react-native-toast-message";
export default function App() {
  return (
    //  <Auth/>
    <SafeAreaProvider>
      <SafeAreaView style={{ flex: 1, backgroundColor: "#07362E" }}>
        <MainRoute />
        <Toast />
      </SafeAreaView>
    </SafeAreaProvider>
  );
}
