import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import React, { useEffect, useState } from "react";
import { StatusBar } from "expo-status-bar";
import { authStyles } from "./authStyles";
import {
  Feather,
  SimpleLineIcons,
  Octicons,
  FontAwesome,
} from "@expo/vector-icons";
import { CircleIcon, signOut, threeLine } from "./../../../assets/images";
import { useNavigation } from "@react-navigation/native";
import Checkbox from "expo-checkbox";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";

const Auth = () => {
  const [activeTab, setActiveTab] = useState("MY_ACCOUNT");
  const navigation = useNavigation();

  const [isChecked, setChecked] = useState(false);
  const handleCheckboxChange = () => {
    setChecked(!isChecked);
  };

  const [secureEntry, setSecureEntry] = useState(true);
  const toggleSecureEntry = () => {
    setSecureEntry(!secureEntry);
  };

  const [savedUser, setSavedUser] = useState(null);
  const [user, setUser] = useState({
    name: "",
    email: "",
    password: "",
  });

  const printInputValues = async () => {
    try {
      const stringData = JSON.stringify(user);
      await AsyncStorage.setItem("userDetail", stringData);
      console.log(user, "User data saved successfully");
      Toast.show({
        type: "success",
        text1: "Success",
        text2: "User data saved successfully",
      });
      setActiveTab("MY_ACCOUNT");
      setSavedUser(user);
    } catch (error) {
      console.log(error, "Error saving data");
    }
  };

  useEffect(() => {
    const retrieveSavedUser = async () => {
      try {
        const savedUserData = await AsyncStorage.getItem("userDetail");
        if (savedUserData !== null) {
          const parsedUser = JSON.parse(savedUserData);
          setSavedUser(parsedUser);
          console.log("Retrieved user data:", parsedUser);
        }
      } catch (error) {
        console.error("Error retrieving user data:", error);
      }
    };
    retrieveSavedUser();
  }, []);
  // console.log(savedUser, "savedUser");
  const error = () => {
    Toast.show({
      type: "error",
      text1: "Error",
      text2: "Error saving data",
    });
  };
  const signInBtn = () => {
    if (
      user?.email === savedUser?.email &&
      user?.password === savedUser?.password
    ) {
      navigation.navigate("TABS");
      console.log("Logged in successfully");
      Toast.show({
        type: "success",
        text1: "Success",
        text2: "Logged in successfully!",
      });
    } else {
      console.log("Invalid email or password");
      Toast.show({
        type: "error",
        text1: "Error",
        text2: "Invalid email or password",
      });
    }
    // const savedUserData = AsyncStorage.removeItem("userDetail");
    // console.log(user, savedUser);
  };

  return (
    <View style={authStyles.container}>
      <View style={{ position: "relative" }}>
        <Text style={authStyles.titleText}>Sign In</Text>
        <Image
          source={CircleIcon}
          style={{ position: "absolute", top: 0, right: "5%" }}
        />
        <Text style={authStyles.subTitleText}>
          Sign in now to access your exercises and saved music.
        </Text>
        <View style={authStyles.login}>
          <View style={authStyles.switchDiv}>
            <TouchableOpacity
              style={[
                authStyles.switch,
                activeTab == "MY_ACCOUNT" && authStyles.active,
              ]}
              onPress={() => setActiveTab("MY_ACCOUNT")}
            >
              <Text
                style={[
                  authStyles.switchBtn,
                  { color: activeTab == "MY_ACCOUNT" ? "white" : "#219653" },
                ]}
              >
                My Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                authStyles.switch,
                activeTab == "REGISTER" && authStyles.active,
              ]}
              onPress={() => setActiveTab("REGISTER")}
            >
              <Text
                style={[
                  authStyles.switchBtn,
                  { color: activeTab == "REGISTER" ? "white" : "#219653" },
                ]}
              >
                Register
              </Text>
            </TouchableOpacity>
          </View>
          {activeTab === "REGISTER" && (
            <View style={[{ position: "relative" }]}>
              <View>
                <TextInput
                  placeholder="Full name"
                  placeholderTextColor="#638A84"
                  style={authStyles.input}
                  onChangeText={(e) => setUser({ ...user, name: e })}
                  name="name"
                  value={user.name}
                />
                <FontAwesome
                  name="user-o"
                  size={20}
                  color="#C2D2CF"
                  style={authStyles.leftIcon1}
                />
              </View>
            </View>
          )}
          <View style={{ position: "relative" }}>
            <TextInput
              placeholder="Enter your email"
              placeholderTextColor="#638A84"
              style={authStyles.input}
              onChangeText={(e) => setUser({ ...user, email: e })}
              name="email"
              value={user.email}
            />
            <Feather
              name="mail"
              size={20}
              color="#C2D2CF"
              style={authStyles.leftIcon1}
            />
          </View>
          <View style={{ position: "relative" }}>
            <TextInput
              placeholder="Enter your Password"
              placeholderTextColor="#638A84"
              style={authStyles.input}
              name="password"
              onChangeText={(e) => setUser({ ...user, password: e })}
              value={user.password}
              secureTextEntry={secureEntry}
            />
            <SimpleLineIcons
              name="lock"
              size={20}
              color="#C2D2CF"
              style={authStyles.leftIcon1}
            />
            <TouchableOpacity
              onPress={toggleSecureEntry}
              style={authStyles.leftIcon3}
            >
              <Octicons
                name={secureEntry ? "eye" : "eye-closed"}
                size={20}
                color="#C2D2CF"
              />
            </TouchableOpacity>
          </View>
          {activeTab === "MY_ACCOUNT" && (
            <Text style={authStyles.passwordText}>Forgot password ?</Text>
          )}
          {activeTab === "REGISTER" && (
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Checkbox
                color={"#C2D2CF"}
                style={{ borderRadius: 7, marginBottom: 5 }}
                value={isChecked}
                onValueChange={handleCheckboxChange}
              />
              <Text style={authStyles.passwordText}>
                I accepted Terms & Privacy Policy
              </Text>
            </View>
          )}
          <TouchableOpacity
            style={authStyles.button}
            onPress={() =>
              activeTab === "REGISTER"
                ? isChecked
                  ? printInputValues()
                  : error()
                : signInBtn()
            }
          >
            <Text style={authStyles.btnText}>
              {activeTab === "MY_ACCOUNT" && "Sign In"}
              {activeTab === "REGISTER" && "Continue"}
            </Text>
          </TouchableOpacity>
        </View>
        <Image
          source={threeLine}
          style={{ position: "absolute", bottom: -100, right: 0 }}
        />
      </View>
      <StatusBar style="auto" />
    </View>
  );
};

export default Auth;
