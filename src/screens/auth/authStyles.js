import { StyleSheet } from "react-native";
export const authStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    // justifyContent: "center",
    paddingHorizontal: 10,
    backgroundColor: "rgba(7, 54, 46, 1)",
    paddingTop: 85,
  },

  login: {
    backgroundColor: "rgba(13, 71, 61, 1)",
    borderRadius: 30,
    // height: 390,
    width: 335,
    padding: 20,
  },

  titleText: {
    color: "white",
    fontSize: 36,
    fontWeight: 700,
  },

  subTitleText: {
    color: "#D8E2E0",
    fontSize: 22,
    fontWeight: 400,
    paddingTop: 24,
    paddingBottom: 50,
  },

  switchDiv: {
    color: "#638A84",
    height: 60,
    marginBottom: 10,
    borderRadius: 15,
    padding: 6,
    backgroundColor: "#08352E",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 14,
  },

  switch: {
    height: "100%",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    width: "50%",
  },
  active: {
    backgroundColor: "#219653",
  },
  switchBtn: {
    fontSize: 14,
    fontWeight: 700,
  },

  input: {
    borderColor: "rgba(99, 138, 132, 1)",
    color: "#638A84",
    borderWidth: 1,
    height: 64,
    marginBottom: 10,
    borderRadius: 16,
    paddingHorizontal: 50,
  },
  leftIcon1: {
    position: "absolute",
    top: "30%",
    left: "5%",
  },
  leftIcon3: {
    position: "absolute",
    top: "30%",
    right: "5%",
  },
  passwordText: {
    textAlign: "right",
    color: "#D8E2E0",
    paddingTop: 10,
    paddingBottom: 20,
    fontSize: 15,
    fontWeight: 700,
  },

  button: {
    backgroundColor: "#FEC265",
    height: 64,
    borderRadius: 16,
    alignItems: "center",
    justifyContent: "center",
  },

  btnText: {
    color: "#141333",
    fontSize: 16,
    fontWeight: 700,
  },
});
