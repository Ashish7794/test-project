import { StyleSheet } from "react-native";

export const eventsStyles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: "#07362E",
      justifyContent: 'center',
      alignItems: 'center',
   },

   text: {
      color: "#fff",
      fontSize: 30,
   },
});
