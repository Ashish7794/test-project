import { View, Text, ScrollView } from "react-native";
import React from "react";
import { eventsStyles } from "./eventsStyles";
import ImageGAllery from "../../component/imageGallery";

const Event = () => {
  return (
    <>
      {/* <ScrollView> */}
      <ImageGAllery />
      <View style={eventsStyles.container}>
        <Text style={eventsStyles.text}>Event</Text>
      </View>
      {/* </ScrollView> */}
    </>
  );
};

export default Event;
