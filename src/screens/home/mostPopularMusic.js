import { View, Text, TouchableOpacity, Image, ScrollView } from "react-native";
import React from "react";
import { homeStyle } from "./homeStyles";
import { SCREEN_NAME } from "../../routing/screenNames";
import {
  backIcon,
  settingsIcon,
  image1,
  image2,
  image3,
  image4,
  image5,
  image6,
} from "../../../assets/images";
import { useNavigation } from "@react-navigation/native";
import MusicBanner from "../../component/musicBanner";
import Button from "../../component/button";
import TopHeader from "../../component/topHeader";

const MostPopularMusic = () => {
  const navigation = useNavigation();

  const Banner_Data = [
    {
      id: 1,
      icon: image1,
      title: "Midnight & relaxation",
    },
    {
      id: 2,
      icon: image2,
      title: "Jogging and cycling",
    },
    {
      id: 3,
      icon: image3,
      title: "Midnight Launderetee",
    },
    {
      id: 4,
      icon: image4,
      title: "Jogging and cycling",
    },
    {
      id: 5,
      icon: image5,
      title: "Tickle Me Pink",
    },
    {
      id: 6,
      icon: image6,
      title: "Michael in the Bathroom",
    },
  ];

  return (
    <ScrollView>
      <View style={homeStyle.container}>
        <TopHeader
          title={"Most Popular"}
          leftClick={() => navigation.navigate(SCREEN_NAME.tab)}
        />
        <View style={[homeStyle.lastDiv, homeStyle.mt60]}>
          {Banner_Data.map((data, index) => {
            return (
              <View style={{ width: "48%" }} key={index}>
                <MusicBanner title={data.title} img={data.icon} />
              </View>
            );
          })}
        </View>
        <View style={{ paddingVertical: 1 }}>
          <Button
            text={"Listing"}
            onClick={() => navigation.navigate(SCREEN_NAME.listing)}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default MostPopularMusic;
