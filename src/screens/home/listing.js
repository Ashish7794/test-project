import {
  View,
  Text,
  useWindowDimensions,
  Image,
  TouchableOpacity,
} from "react-native";
import React from "react";
import { homeStyle } from "./homeStyles";
import TopHeader from "../../component/topHeader";
import { SCREEN_NAME } from "../../routing/screenNames";
import { useNavigation } from "@react-navigation/native";
import Carousel from "react-native-snap-carousel";
import {
  image1,
  listingBar,
  nextIcon,
  prevIcon,
  playIcon2,
  repeatIcon,
  rotate_ccw,
} from "../../../assets/images";

const Listing = () => {
  const navigation = useNavigation();

  return (
    <View style={homeStyle.container}>
      <TopHeader
        title={"Listing"}
        leftClick={() => navigation.navigate(SCREEN_NAME.popularMusic)}
      />
      <View style={homeStyle.sliderSection}>
        <Carousel
          data={["n", "h", "j", "n", "h", "j"]}
          renderItem={() => (
            <View>
              <Image source={image1} style={{ width: 223, height: 271 }} />
            </View>
          )}
          sliderWidth={useWindowDimensions().width}
          itemWidth={223}
        />
      </View>
      <View>
        <Text style={homeStyle.listing}>Michael in the Bathroom</Text>
        <Text style={homeStyle.subListing}>
          &#128517; Worried By Joe Iconis
        </Text>
      </View>
      <View style={homeStyle.listingGroup}>
        <Text style={homeStyle.listingBar}>01:42</Text>
        <Image source={listingBar} style={{ marginHorizontal: 9 }} />
        <Text style={homeStyle.listingBar}>02:30</Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <View style={homeStyle.playControl}>
          <TouchableOpacity>
            <Image source={repeatIcon} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={prevIcon} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={playIcon2} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={nextIcon} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={rotate_ccw} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Listing;
