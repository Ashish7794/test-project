import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  StatusBar,
} from "react-native";
import {
  menuIcon,
  profile,
  searchIcon,
  stressRelief,
  sleeping,
  dissapointed,
  relieved,
  focusedImg,
  image1,
  image2,
  playIcon,
} from "../../../assets/images";
import React from "react";
import { homeStyle } from "./homeStyles";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import Network from "../network";
import Event from "../events";
import { FontAwesome5 } from "@expo/vector-icons";
import MusicBanner from "../../component/musicBanner";

const Home = () => {
  const STEPS = [
    {
      id: 1,
      name: "Stress Relief ",
      icon: stressRelief,
    },
    {
      id: 2,
      name: "Sleeping",
      icon: sleeping,
    },
    {
      id: 3,
      name: "Dissapointed",
      icon: dissapointed,
    },
    {
      id: 4,
      name: "Relieved",
      icon: relieved,
    },
  ];
  const navigation = useNavigation();

  return (
    <ScrollView>
      <StatusBar barStyle={"light-content"} />
      <View style={homeStyle.container}>
        <View style={homeStyle.topIcons}>
          <View>
            <TouchableOpacity>
              <Image source={profile} />
            </TouchableOpacity>
            <Text style={homeStyle.nameText}>Hi Ashish!</Text>
            <Text style={homeStyle.subText}>How are you feeling today ?</Text>
          </View>
          <View style={homeStyle.borderDiv}>
            <TouchableOpacity style={{ paddingBottom: 4 }}>
              <Image source={menuIcon} />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image source={searchIcon} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={homeStyle.stepDiv}>
          {STEPS.map((data, index) => {
            return (
              <View key={index}>
                <TouchableOpacity style={homeStyle.center}>
                  <View style={homeStyle.stepSubDiv}>
                    <Image source={data.icon} />
                  </View>
                  <Text style={homeStyle.stepText}>{data.name}</Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
        <View style={homeStyle.updateDiv}>
          <Text style={homeStyle.updateHeading}>Latest update</Text>
          <Text style={homeStyle.seeAll}>See all</Text>
        </View>
        <View style={homeStyle.listenNow}>
          <View style={{ width: "60%" }}>
            <Text style={homeStyle.focusedText}>Focused & Mindfulness</Text>
            <Text style={homeStyle.stepText}>
              involves focusing on something intently as a way of staying.
            </Text>
            <TouchableOpacity style={homeStyle.btn}>
              <FontAwesome5 name="play" size={9} color="#F7F8FA" />
              <Text style={homeStyle.btnText}>Listen now</Text>
            </TouchableOpacity>
          </View>
          <View style={{ position: "relative" }}>
            <Image
              source={focusedImg}
              style={{ position: "absolute", left: -13 }}
            />
          </View>
        </View>
        <View style={homeStyle.lastDiv}>
          <View style={{ width: "48%" }}>
            <View>
              <Text style={homeStyle.updateHeading}>Most</Text>
              <Text style={homeStyle.updateNextHeading}>Popular</Text>
            </View>
            <View style={{ marginTop: 26, position: "relative" }}>
              <Image
                source={playIcon}
                style={{ position: "absolute", top: 20, left: 16, zIndex: 9 }}
              />
              <Image
                source={image1}
                style={{ width: "100%", borderRadius: 30 }}
              />
              <Text style={homeStyle.imgContent}>Focused & Mindfulness</Text>
            </View>
          </View>
          <View style={{ width: "48%" }}>
            <MusicBanner title={"Jogging and cycling"} img={image2} />
            <TouchableOpacity
              onPress={() => navigation.navigate("MostPopularMusic")}
            >
              <View style={homeStyle.exploreBtn}>
                <Text style={homeStyle.exploreBtnText}>Explore more</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Home;
