import { StyleSheet } from "react-native";

export const profileStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07362E",
    paddingVertical: 35,
    paddingHorizontal: 16,
  },
  topHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  heading: {
    fontSize: 24,
    fontWeight: 700,
    color: "#FEC265",
  },
  profileDiv: {
    borderRadius: 25,
    border: 1,
    backgroundColor: "#0D473D",
    padding: 16,
    marginVertical: 36,
    flexDirection: "row",
    alignItems: "center",
    position: "relative",
  },
  userName: {
    fontSize: 24,
    fontWeight: 700,
    color: "#FFF",
    paddingLeft: 16,
    paddingBottom: 12,
  },
  userMail: {
    fontSize: 18,
    fontWeight: 700,
    color: "#C2D2CF",
    paddingLeft: 16,
  },
  daily: {
    fontSize: 24,
    fontWeight: 700,
    color: "#FFF",
  },
  progress: {
    fontSize: 24,
    fontWeight: 700,
    color: "#FEC265",
  },
  dailyProgress: {
    borderRadius: 19,
    border: 1,
    backgroundColor: "#0D473D",
    paddingHorizontal: 12,
    marginTop: 24,
    flexDirection: "row",
    alignItems: "center",
    width: "47%",
    paddingVertical: 22,
    position: "relative",
    flexWrap: "wrap",
  },
  dailyProgressDiv: {
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 36,
  },
  count: {
    fontSize: 18,
    fontWeight: 700,
    color: "#FFFFFF",
  },
  countText: {
    fontSize: 16,
    fontWeight: 700,
    color: "#C2D2CF",
    paddingTop: 8,
  },
  weekDay: {
    fontSize: 16,
    fontWeight: 600,
    color: "#C2D2CF",
  },
  date: {
    fontSize: 16,
    fontWeight: 700,
    color: "#EAEFEE",
  },
  settingHeading: {
    fontSize: 20,
    fontWeight: 700,
    color: "#fff",
  },
  settingHeading1: {
    fontSize: 20,
    fontWeight: 700,
    color: "#FEC265",
    paddingBottom: 6,
  },
});
