import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import React, { Component, useEffect, useState } from "react";
import { profileStyles } from "./profileStyles";
import {
  backIcon,
  settingsIcon,
  profile,
  progressIcon1,
  progressIcon2,
  progressIcon3,
  progressIcon4,
  Back,
  Next,
  Chart,
} from "../../../assets/images";
import { useNavigation } from "@react-navigation/native";
import { Feather } from "@expo/vector-icons";

const Profile = () => {
  const navigation = useNavigation();
  const PROGRESS = [
    {
      id: 1,
      count: "135h",
      countText: "Total meditation",
      countIcon: progressIcon1,
    },
    {
      id: 2,
      count: "45 min",
      countText: "Longest listening",
      countIcon: progressIcon2,
    },
    {
      id: 3,
      count: "4h 23m",
      countText: "Deep sleep",
      countIcon: progressIcon3,
    },
    {
      id: 4,
      count: "2h 5m",
      countText: "Light sleep",
      countIcon: progressIcon4,
    },
  ];

  const [data, setData] = useState("");
  const [loading, setLoading] = useState(true);
  console.log(data, "datadata");
  const fetchData = async () => {
    const resp = await fetch(
      "http://3.216.234.145:5001/v1/users/6450fea1a66723f5ef915363"
    );
    const data = await resp.json();
    console.log(data, "resp");
    setData(data?.result?.info);
    setLoading(false);
  };
  console.log(data, "info");

  useEffect(() => {
    fetchData();
  }, []);

  const [onClick, setOnClick] = useState(false);
  const handleToggleClick = () => {
    setOnClick(!onClick);
  };

  return (
    <ScrollView>
      <View style={profileStyles.container}>
        <View style={profileStyles.topHeader}>
          <TouchableOpacity>
            <Image source={backIcon} />
          </TouchableOpacity>
          <Text style={profileStyles.heading}>My profile</Text>
          <TouchableOpacity onPress={() => navigation.navigate("SETTING")}>
            <Image source={settingsIcon} />
          </TouchableOpacity>
        </View>
        <View style={profileStyles.profileDiv}>
          <Image source={profile} style={{ height: 99, width: 99 }} />
          <View style={{ maxWidth: "75%", overflow: "hidden" }}>
            <Text style={profileStyles.userName}>
              {data?.firstName} {data?.lastName}
              {/* Ashish Yadav */}
            </Text>
            <Text style={profileStyles.userMail}>
              {data?.mobile}
              {/* 9453256852 */}
            </Text>
            <TouchableOpacity onPress={handleToggleClick}>
              <Text
                style={profileStyles.userMail}
                numberOfLines={onClick ? 3 : 1}
                ellipsizeMode="tail"
              >
                {data?.email}
                {/* ashish@bloohash.com */}
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{ position: "absolute", top: 10, right: 10 }}
          >
            <Feather name="edit" size={20} color="#C2D2CF" />
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Text style={profileStyles.daily}>Daily </Text>
          <Text style={profileStyles.progress}>progress</Text>
        </View>
        <View style={profileStyles.dailyProgressDiv}>
          {PROGRESS.map((data, index) => {
            return (
              <View style={profileStyles.dailyProgress} key={index}>
                <Text style={profileStyles.count}> {data.count}</Text>
                <Text style={profileStyles.countText}> {data.countText}</Text>
                <Image
                  source={data.countIcon}
                  style={{ position: "absolute", top: 8, right: 8 }}
                />
              </View>
            );
          })}
        </View>
        <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text style={profileStyles.daily}>Your </Text>
              <Text style={profileStyles.progress}>Insights</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={profileStyles.weekDay}>This Week: </Text>
              <Text style={profileStyles.date}>05may- 11 may</Text>
            </View>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Image source={Back} />
            <Image source={Next} style={{ marginLeft: 12 }} />
          </View>
        </View>
        <Image
          source={Chart}
          style={{ width: "100%", borderRadius: 25, marginTop: 32 }}
        />
      </View>
    </ScrollView>
  );
};

export default Profile;
