import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import React from "react";
import { profileStyles } from "./profileStyles";
import {
  settingBG,
  settingsIcon,
  accountInformation,
  notifications,
  dailyReminders,
  privacyPolicy,
  termsService,
  signOut,
} from "../../../assets/images";

const Setting = () => {
  const SETTING_DATA = [
    {
      id: 1,
      icon: accountInformation,
      text: "Account Information",
    },
    {
      id: 2,
      icon: notifications,
      text: "Notifications",
    },
    {
      id: 3,
      icon: dailyReminders,
      text: "Daily Reminders",
    },
    {
      id: 4,
      icon: privacyPolicy,
      text: "Privacy Policy",
    },
    {
      id: 5,
      icon: termsService,
      text: "Terms of Service",
    },
    {
      id: 6,
      icon: signOut,
      text: "Sign Out",
    },
  ];
  return (
    <View style={profileStyles.container}>
      <View style={{ position: "relative" }}>
        <ImageBackground
          resizeMode="stretch"
          source={settingBG}
          style={{ width: "100%", height: "94%" }}
        >
          <View style={{ padding: 32 }}>
            <Text style={profileStyles.settingHeading}>Manage your</Text>
            <Text style={profileStyles.settingHeading1}>Settings</Text>
            <TouchableOpacity
              style={{ position: "absolute", right: 0, top: 0 }}
            >
              <Image source={settingsIcon} />
            </TouchableOpacity>
            {SETTING_DATA.map((data, index) => {
              return (
                <View
                  key={index}
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 30,
                  }}
                >
                  <TouchableOpacity>
                    <Image source={data.icon} style={{ marginRight: 16 }} />
                  </TouchableOpacity>
                  <Text style={profileStyles.settingHeading}>{data.text}</Text>
                </View>
              );
            })}
          </View>
        </ImageBackground>
      </View>
    </View>
  );
};

export default Setting;
