import React, { useReducer, useState } from "react";
import { View, Text, TouchableOpacity, TextInput } from "react-native";
import Checkbox from "expo-checkbox";
import { networkStyle } from "./networkStyles";
import { authStyles } from "../auth/authStyles";
import { MotiView } from "moti"; 

const Network = () => {
  const [activeTab, setActiveTab] = useState("MY_ACCOUNT");

  return (
    <View style={networkStyle.container}>
      <MotiView
        from={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ loop: true, type: "timing", duration: 1500, delay: 100 }}
      >
        <Text style={networkStyle.text}>Network</Text>
      </MotiView>
      <View style={networkStyle.animationDiv}>
        <View>
          <View style={authStyles.switchDiv}>
            <TouchableOpacity
              style={[
                authStyles.switch,
                activeTab == "MY_ACCOUNT" && authStyles.active,
              ]}
              onPress={() => setActiveTab("MY_ACCOUNT")}
            >
              <Text
                style={[
                  authStyles.switchBtn,
                  { color: activeTab == "MY_ACCOUNT" ? "white" : "#219653" },
                ]}
              >
                My Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                authStyles.switch,
                activeTab == "REGISTER" && authStyles.active,
              ]}
              onPress={() => setActiveTab("REGISTER")}
            >
              <Text
                style={[
                  authStyles.switchBtn,
                  { color: activeTab == "REGISTER" ? "white" : "#219653" },
                ]}
              >
                Register
              </Text>
            </TouchableOpacity>
          </View>
          <MotiView
            from={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{
              type: "timing",
              duration: 2500,
            }}
          >
            {activeTab === "REGISTER" && (
              <View style={[{ position: "relative" }]}>
                <View>
                  <TextInput
                    placeholder="Full name"
                    placeholderTextColor="#638A84"
                    style={authStyles.input}
                    name="name"
                  />
                </View>
              </View>
            )}
          </MotiView>
          <View style={{ position: "relative" }}>
            <TextInput
              placeholder="Enter your email"
              placeholderTextColor="#638A84"
              style={authStyles.input}
              name="email"
            />
          </View>
          <View style={{ position: "relative" }}>
            <TextInput
              placeholder="Enter your Password"
              placeholderTextColor="#638A84"
              style={authStyles.input}
              name="password"
            />
          </View>
          {activeTab === "MY_ACCOUNT" && (
            <Text style={authStyles.passwordText}>Forgot password?</Text>
          )}
          {activeTab === "REGISTER" && (
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Checkbox
                color={"#C2D2CF"}
                style={{ borderRadius: 7, marginBottom: 5 }}
              />
              <Text style={authStyles.passwordText}>Privacy Policy</Text>
            </View>
          )}
          <TouchableOpacity style={authStyles.button}>
            <Text style={authStyles.btnText}>
              {activeTab === "MY_ACCOUNT" && "Sign In"}
              {activeTab === "REGISTER" && "Continue"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Network;
