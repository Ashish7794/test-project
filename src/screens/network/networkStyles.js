import { StyleSheet } from "react-native";

export const networkStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07362E", 
    paddingHorizontal: '10%',
  },

  text: {
    color: "#fff",
    fontSize: 30,
  },
  animationDiv: { 
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(13, 71, 61, 1)",
    borderRadius: 30, 
    padding: 20,
  },
});
