import { StyleSheet } from "react-native";

export const componentStyle = StyleSheet.create({
  imgContent: {
    fontSize: 18,
    fontWeight: 700,
    color: "#FFF",
    position: "absolute",
    bottom: 20,
    left: 20,
  },

  button: {
    backgroundColor: "#FEC265",
    height: 64,
    borderRadius: 16,
    alignItems: "center",
    justifyContent: "center",
  },

  btnText: {
    color: "#141333",
    fontSize: 16,
    fontWeight: 700,
  },
  topHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  heading: {
    fontSize: 24,
    fontWeight: 700,
    color: "#FEC265",
  },
});
