import { View, Text, TouchableOpacity, Image } from "react-native";
import React from "react";
import { componentStyle } from "./componentStyle";
import { backIcon, settingsIcon } from "../../assets/images";

const TopHeader = (props) => {
  const { title, leftClick, rightClick } = props;
  return (
    <View style={componentStyle.topHeader}>
      <TouchableOpacity onPress={leftClick}>
        <Image source={backIcon} />
      </TouchableOpacity>
      <Text style={componentStyle.heading}>{title}</Text>
      <TouchableOpacity onPress={rightClick}>
        <Image source={settingsIcon} />
      </TouchableOpacity>
    </View>
  );
};

export default TopHeader;
