import { View, Text, Image, TouchableOpacity } from "react-native";
import React from "react";
import { playIcon } from "../../assets/images";
import { componentStyle } from "./componentStyle";

const MusicBanner = (props) => {
  const { img, title } = props;
  return (
    <View>
      <View style={{ position: "relative" }}>
        <TouchableOpacity
          style={{ position: "absolute", top: 20, left: 16, zIndex: 9 }}
        >
          <Image source={playIcon} />
        </TouchableOpacity>
        <Image source={img} style={{ width: "100%", borderRadius: 30 }} />
        <Text style={componentStyle.imgContent}>{title}</Text>
      </View>
    </View>
  );
};

export default MusicBanner;
