import { StyleSheet, useWindowDimensions } from "react-native";

export const imgGalleryStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07362E",
    paddingVertical: 50,
  },
  sliderContainer: {
    height: 200,
    marginTop: 20,
  },
  image: {
    // width: 390,
    height: 200,
    // marginRight: 10,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
  },
  buttonText: {
    padding: 10,
    fontSize: 16,
    color: "blue",
  },
  button: {
    backgroundColor: "#219653",
    padding: 10,
    borderRadius: 10,
  },
});
