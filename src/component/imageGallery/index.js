import React, { useState, useRef, useEffect } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  useWindowDimensions,
} from "react-native";
import { imgGalleryStyle } from "./imgGalleryStyle";
import {
  image1,
  image2,
  image3,
  image4,
  image5,
  image6,
} from "../../../assets/images";

const ImageGallery = () => {
  const scrollViewRef = useRef(null);
  const [currentIndex, setCurrentIndex] = useState(0);

  const handleNext = () => {
    if (currentIndex === images.length - 1) {
      goTo(0);
    } else {
      if (currentIndex < images.length - 1) {
        setCurrentIndex(currentIndex + 1);
        scrollViewRef.current.scrollTo({ x: (currentIndex + 1) * imageWidth });
      }
    }
  };

  const handlePrevious = () => {
    if (currentIndex === 0) {
      goTo(images.length - 1);
    } else {
      if (currentIndex > 0) {
        setCurrentIndex(currentIndex - 1);
        scrollViewRef.current.scrollTo({ x: (currentIndex - 1) * imageWidth });
      }
    }
  };

  useEffect(() => {
    // Automatically scroll to the next image every scrollInterval
    const interval = setInterval(() => {
      handleNext();
    }, 1000);

    return () => {
      clearInterval(interval); // Clear the interval on component unmount
    };
  }, [currentIndex]);

  const images = [
    { id: 1, img: image1 },
    { id: 2, img: image2 },
    { id: 3, img: image3 },
    { id: 4, img: image4 },
    { id: 5, img: image5 },
    { id: 6, img: image6 },
  ];

  const imageWidth = useWindowDimensions().width;

  const goTo = (index) => {
    setCurrentIndex(index);
    scrollViewRef.current.scrollTo({ x: index * imageWidth });
  };

  const renderImages = (h, w) => {
    return images.map((data, index) => {
      return (
        <Image
          key={index}
          source={data.img}
          resizeMode="contain"
          style={[imgGalleryStyle.image, { width: w, height: h }]}
        />
      );
    });
  };

  const pagingImages = (h, w) => {
    return images.map((data, index) => {
      return (
        <TouchableOpacity
          key={index}
          onPress={() => {
            goTo(index);
          }}
        >
          <Image
            key={index}
            source={data.img}
            resizeMode="contain"
            style={[
              imgGalleryStyle.image,
              {
                width: w,
                height: h,
                borderWidth: currentIndex === index ? 2 : 0,
                borderColor: "gray",
                marginHorizontal: 5,
              },
            ]}
          />
        </TouchableOpacity>
      );
    });
  };

  return (
    <View style={imgGalleryStyle.container}>
      <ScrollView
        ref={scrollViewRef}
        horizontal
        style={imgGalleryStyle.sliderContainer}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        onMomentumScrollEnd={(event) => {
          const contentOffset = event.nativeEvent.contentOffset;
          const viewSize = event.nativeEvent.layoutMeasurement;
          const index = Math.floor(contentOffset.x / viewSize.width);
          console.log(index, currentIndex);
          setCurrentIndex(index);

          //   if (index + 1 === images.length) {
          //     setCurrentIndex(0);
          //     goTo(0);
          //   } else {
          //   }
        }}
      >
        {renderImages(200, imageWidth)}
      </ScrollView>
      <View style={imgGalleryStyle.buttonContainer}>
        <TouchableOpacity onPress={handlePrevious}>
          <Text style={imgGalleryStyle.buttonText}>Previous</Text>
        </TouchableOpacity>
        <Text>
          {currentIndex + 1}/{images.length}
        </Text>
        <TouchableOpacity onPress={handleNext}>
          <Text style={imgGalleryStyle.buttonText}>Next</Text>
        </TouchableOpacity>
      </View>
      <ScrollView inf horizontal showsHorizontalScrollIndicator={false}>
        {pagingImages(50, 50)}
      </ScrollView>
    </View>
  );
};

export default ImageGallery;
