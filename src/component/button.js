import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import { componentStyle } from "./componentStyle";

const Button = (props) => {
  const { text, onClick } = props;
  return (
    <View>
      <TouchableOpacity style={componentStyle.button} onPress={onClick}>
        <Text style={componentStyle.btnText}>{text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Button;
