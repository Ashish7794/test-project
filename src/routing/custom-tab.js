import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';

const TABS = [
   {
      id: 1,
      name: "Home",
      icon: "home",
   },
   {
      id: 2,
      name: "Network",
      icon: "network-wired",
   },
   {
      id: 3,
      name: "Event",
      icon: "font-awesome",
   },
   {
      id: 4,
      name: "Profile",
      icon: "user",
   },
]

const CustomTabBar = (props) => {

   const { state } = props
   // console.log(props, "ddddddd", state?.routeNames[0] == "Home");
   return (
      <View style={styles.container}>
         {TABS.map((data, index) => {
            return (
               <View key={index}>
                  <TouchableOpacity style={[styles.tabItemStyle, { backgroundColor: state?.routeNames[state?.index] == data.name ? "#07362E" : "transparent" }]} onPress={() => props?.navigation.navigate(data.name)}>
                     <FontAwesome5 name={data.icon} size={24} color="white" />
                     <Text style={{ color: 'white' }}>{data.name}</Text>
                  </TouchableOpacity>
               </View>
            )
         })}
      </View>
   )
}
export default CustomTabBar

const styles = StyleSheet.create({
   container: {
      backgroundColor: '#219653',
      flexDirection: 'row',
      justifyContent: 'space-around',
      paddingVertical: 10,
   },
   tabItemStyle: {
      alignItems: 'center',
      padding: 5,
      borderRadius: 15,
      width: 75
   },
})