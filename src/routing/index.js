import { View, Text } from "react-native";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../screens/home";
import Auth from "../screens/auth";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import TabNavigator from "./tabNavigator";
import Setting from "../screens/profile/setting";
import { SCREEN_NAME } from "./screenNames";
import MostPopularMusic from "../screens/home/mostPopularMusic";
import Listing from "../screens/home/listing";
import ImageGAllery from "../component/imageGallery";
const Stack = createNativeStackNavigator();
const MainRoute = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name={SCREEN_NAME.auth}
          component={Auth}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={SCREEN_NAME.home}
          component={Home}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="TABS"
          component={TabNavigator}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SETTING"
          component={Setting}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={SCREEN_NAME.popularMusic}
          component={MostPopularMusic}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={SCREEN_NAME.listing}
          component={Listing}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={SCREEN_NAME.imageGAllery}
          component={ImageGAllery}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainRoute;
