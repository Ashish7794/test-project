export const SCREEN_NAME = {
  home: "HOME",
  auth: "AUTH",
  tab: "TABS",
  setting: "SETTING",
  popularMusic: "MostPopularMusic",
  listing: "LISTING",
  imageGAllery: "ImageGAllery",
};
